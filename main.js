import Vue from 'vue'
import App from './App'
import api from '@/api'
import tool from '@/tool/tool'
import cuCustom from '@/static/colorui/components/cu-custom.vue'
// import router from '@/common/router'
// import '@/js_sdk/ican-H5Api/ican-H5Api'
import global from './api/global.js'
Vue.config.productionTip = false
Vue.component('cu-custom', cuCustom);
Vue.prototype.$api = api
Vue.prototype.$msg = tool.msg
Vue.prototype.$tool = tool
Vue.prototype.global = global
import rfBackHome from '@/components/rf-back-home';
import rfEmpty from '@/components/rf-empty';

import empty from "@/components/empty.vue"
/* 数字键盘组件 */
import payPassword from '@/components/pay-password.vue'
/* confirm组件 */
import modalConfirm from '@/components/modal-confirm.vue'
Vue.component('empty',empty);
Vue.component('payPassword',payPassword);
Vue.component('modalConfirm',modalConfirm);


/* 自定义头部导航 返回事件 */
Vue.prototype.goBack = function(){
	//#ifdef H5
		history.back();
	//#endif
	
	//#ifndef H5
		uni.navigateBack();
	//#endif
}
/* 跳转事件 */
Vue.prototype.jump = function(url){
	uni.navigateTo({
		url:url
	})
}
Vue.prototype.sjump = function(url){
	uni.switchTab({
		url:url
	})
}
App.mpType = 'app'

const app = new Vue({
    ...App
})
// RouterMount(app, '#app');
app.$mount()
